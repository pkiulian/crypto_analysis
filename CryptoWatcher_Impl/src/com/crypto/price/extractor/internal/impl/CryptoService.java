package com.crypto.price.extractor.internal.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import com.crypto.price.extractor.api.*;
import org.osgi.service.component.ComponentContext;
import java.util.Dictionary;


public class CryptoService implements IService {
	
	/**
     * This is a coin name, example OMG, ZEC etc.
     */
    private String m_cryptoName;
    
    
    /**
     * This is the coin id as shown in the REST API : https://api.coinpaprika.com/v1/coins/omg-omg-network/markets?quotes=EUR
     */
    private String m_coinId;

 	/**
 	 * waiting time in seconds between request result from the API
 	 */
 	private int m_refreshTime;
	 


	private String currency = "EUR";

	private volatile double price;
	
	private volatile double oldPrice= 0;
	
	Lock lock = new ReentrantLock();

	static ScheduledExecutorService priceExtractor;


	private List<IServiceListener> listeners = new ArrayList<>();
	
	@Override
	public void setCurrenncyPrice(String currency) {
		this.currency = currency;
	}

	@Override
	public double getCurrentPrice() {
		return this.price;

	}
	
	@Override
	public void addListener(IServiceListener listener) {
		System.out.println("listener is added");
		synchronized (listeners) {
			if (!listeners.contains(listener)) {
				listeners.add(listener);
			}
		}
		
	}

	@Override
	public void removeListener(IServiceListener listener) {
		synchronized (listeners) {
			if (listeners.contains(listener)) {
				listeners.remove(listener);
			}
		}
	}

	@Override
	public void startUpService(ComponentContext context) {
		
		priceExtractor = new ScheduledThreadPoolExecutor(1);
		
		Dictionary<String, Object> properties = context.getProperties();
      
        m_cryptoName = (String) properties.get("cryptoName");
        m_coinId = (String) properties.get("coinId");
        m_refreshTime = Integer.valueOf((String)(properties.get("refreshTime")));
        
        final String OMG_EUR_PRICE = "https://api.coinpaprika.com/v1/coins/"+m_coinId+"/markets?quotes=EUR";
		System.out.println("Starting up the Crypto with "+m_cryptoName);
		
		try {

			URL url = new URL(OMG_EUR_PRICE);
			
			// THIS IS UGLY, but would do it for now this are specifics for what I need,
			// since for this HTTP request I can not impose filters based on the exchange_id
			 String toSearchForStart = "{\"exchange_id\":\"kraken\",\"exchange_name\":\"Kraken\",\"pair\":\""+this.m_cryptoName+"/EUR\"";
			 String toSearchForEnds = "},";
			

			Runnable runTask = new Runnable() {

				@Override
				public void run() {
					StringBuffer content = new StringBuffer();

					try {
						HttpURLConnection con = (HttpURLConnection) url.openConnection();

						con.setDoOutput(true);
						con.setRequestMethod("GET");

						con.setUseCaches(false);

						BufferedReader buffer = new BufferedReader(new InputStreamReader(con.getInputStream()));
						String inputLine = "";
						content = new StringBuffer();
						while ((inputLine = buffer.readLine()) != null) {
							int start = inputLine.indexOf(toSearchForStart);
							int end = inputLine.indexOf(toSearchForEnds, start);

							content.append(inputLine.subSequence(start, end));
						}
						buffer.close();

																										             			          			          
						int priceIndexStart = content.indexOf("\"price\":");						 
						int priceIndexEnds = content.toString().indexOf(",\"",priceIndexStart);						
						System.out.println((new Date()) + " For "+m_cryptoName+" the price in "+CryptoService.this.currency+" is: " + content.toString().substring(priceIndexStart+8, priceIndexEnds));
						lock.lock();
						double currentPrice =  Double.parseDouble(content.toString().substring(priceIndexStart+8, priceIndexEnds));
						if (currentPrice!=oldPrice) {
							price=currentPrice;
							notifyListeners();
							oldPrice=currentPrice;
						}
						lock.unlock();
					
					} catch (IOException e) {
						e.printStackTrace();
					}

				}
			};

			

			priceExtractor.scheduleAtFixedRate(runTask, 0, m_refreshTime, TimeUnit.SECONDS);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	protected void notifyListeners() {
		System.out.println("Listeners are notified ");
		listeners.stream().forEach(l->l.priceChanged());		
	}

	@Override
	public void shutDownService() {		
		priceExtractor.shutdown();
	}

}
