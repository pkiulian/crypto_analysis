package cryptowatcher_ui;

import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Singleton;

import org.eclipse.e4.core.di.annotations.Creatable;
import org.eclipse.swt.widgets.Display;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import com.crypto.price.extractor.api.IService;
import com.crypto.price.extractor.api.IServiceListener;

import cryptowatcher_ui.views.IViewPartAdapter; 


@Creatable
@Singleton
public class PriceChanger implements IServiceListener{
	
	/**
	 * We have two Price Log Views for 2 different Coins. 
	 * One entry is for OMG, priceLogView and the second is for ZEC, proceLogView. 
	 */
	private Map<String, IViewPartAdapter> viewsPriceLog = new HashMap<>();
	/**
	 * We have two Price Charts Views for 2 different Coins. 
	 * One entry is for OMG, chartView and the second is for ZEC, chartView. 
	 */
	private Map<String, IViewPartAdapter> viewsCharts = new HashMap<>();
	
	
	/**
	 * One service for OMG and one for ZEC. The key is the coin id
	 */
	private Map<String, IService> filterServices = new HashMap<>();
	/**
	 * One price for OMG and one for ZEC. They key is the coin id
	 */
	private Map<String, Double>  currentPrices = new HashMap<>();
	
	/**
	 * One price for OMG and one for ZEC. They key is the coin id
	 */
	private Map<String, Double>  oldPrices = new HashMap<>();
	

	
	public void startUp(String filter) throws InvalidSyntaxException{

		if (!this.filterServices.keySet().contains(filter)) {
			
			System.out.println("listener is trying to be added");
			BundleContext context = ModelHolder.getInstance().getBundleContext();

			ServiceReference<?>[] serviceRef = context.getServiceReferences(IService.class.getName(), filter);

			IService cryptoService = (IService) context.getService(serviceRef[0]);
			
			this.filterServices.put(filter,  cryptoService);
			
			
			currentPrices.put(filter , 0d);
			oldPrices.put(filter, 0d);

					
			cryptoService.addListener(this);
		}
	}

	public void shutDown() {		
		 Iterator<Entry<String, IService>> it = this.filterServices.entrySet().iterator();
		 while (it.hasNext()) {
			 Map.Entry<String, IService> pair = (Entry<String, IService>)it.next();
			 pair.getValue().removeListener(this);			 
		 }

	}
	
	@Override
	public void priceChanged() {
		System.out.println("Indded the listener is notified ");		
		
		 Iterator<Entry<String, IService>> it = this.filterServices.entrySet().iterator();
		 while (it.hasNext()) {
			 Map.Entry<String, IService> pair = (Entry<String, IService>)it.next();			 	
			 currentPrices.put(pair.getKey(), pair.getValue().getCurrentPrice());
		 }

	
		 // the main part of the class 
		Display.getDefault().syncExec(new Runnable() {		    
			public void run() {

				Iterator<Entry<String, IViewPartAdapter>> it1 = PriceChanger.this.viewsPriceLog.entrySet().iterator();			
				Iterator<Entry<String, IViewPartAdapter>> it2 = PriceChanger.this.viewsCharts.entrySet().iterator();
				while (it1.hasNext() && it2.hasNext()) {
					 Map.Entry<String, IViewPartAdapter> pairPriceLog = (Entry<String, IViewPartAdapter>)it1.next();
					 Map.Entry<String, IViewPartAdapter> pairPriceChart = (Entry<String, IViewPartAdapter>)it2.next();	
					 // only new prices considered
					 if (PriceChanger.this.getCurrentPrice(pairPriceLog.getKey()) != PriceChanger.this.getOldPrice(pairPriceLog.getKey())){
						 pairPriceLog.getValue().appendText("Current crypto price: " +PriceChanger.this.getCurrentPrice(pairPriceLog.getKey()) + " \n ");
						 PriceChanger.this.oldPrices.put(pairPriceLog.getKey(), PriceChanger.this.getCurrentPrice(pairPriceLog.getKey()));
					 }
					 
					 ActionEvent event = new ActionEvent(pairPriceLog.getValue(), 0, "Current crypto void price: " + PriceChanger.this.getCurrentPrice(pairPriceLog.getKey()) + " \n ");
					 pairPriceChart.getValue().actionPerformed(event);
				}
								
		    }
		});		
		
		
	}
	
	public IService getCryptoService(String key) {
		return this.filterServices.get(key);
	}

	
	public void setView1(String key, IViewPartAdapter view1) {
		this.viewsPriceLog.put(key, view1);		
	}

	public void setView2(String key, IViewPartAdapter view2) {
		this.viewsCharts.put(key, view2);		
	}
	
	public double getCurrentPrice(String key) {
		return currentPrices.get(key);
	}
	
	public double getOldPrice(String key) {
		return oldPrices.get(key);
	}

}
