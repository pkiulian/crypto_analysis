package cryptowatcher_ui;

import org.osgi.framework.BundleContext;

public class ModelHolder {

	private static ModelHolder instance;
	
	private BundleContext bundleContext;
	
	public static ModelHolder getInstance() {
		if (instance==null) {
			instance =  new ModelHolder();			
		}
		return instance;
	}
	
	private ModelHolder() {
		
	}

	public void addBundleContext(BundleContext context) {
		this.bundleContext = context;		
	}

	public void removeBundleContext(BundleContext context) {
		this.bundleContext = null;		
	}

	public BundleContext getBundleContext() {
		return this.bundleContext;		
	}

}
