package cryptowatcher_ui.views;

import java.awt.event.ActionListener;

public interface IViewPartAdapter  extends ActionListener{

	
	public void appendText(String string);

}
