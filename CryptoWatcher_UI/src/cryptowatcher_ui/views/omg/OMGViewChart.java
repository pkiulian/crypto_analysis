package cryptowatcher_ui.views.omg;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.inject.Inject;
import javax.swing.JPanel;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.osgi.framework.InvalidSyntaxException;

import cryptowatcher_ui.PriceChanger;
import cryptowatcher_ui.views.IViewPartAdapter;

public class OMGViewChart extends ViewPart implements IViewPartAdapter {
	public static final String ID = "cryptowatcher_ui.omgviewchart";

	
//	private String filter_ZEC = "(coinId=zec-zcash)";
	private String filter_OMG = "(coinId=omg-omg-network)";

	@Inject
	PriceChanger dc;

	static GraphicsConfiguration gc;

	/** The time series data. */
	private TimeSeries series;

	/** The most recent value added. */
	private double lastValue = 0.0;
	
	private double currenValue = 0.0;

	@Override
	public void createPartControl(Composite parent) {

		

		Composite viewComposite = new Composite(parent, SWT.EMBEDDED);
		final Frame frame = SWT_AWT.new_Frame(viewComposite);

		this.series = new TimeSeries("Time Data");

		final TimeSeriesCollection dataset = new TimeSeriesCollection(this.series);
		final JFreeChart chart = createChart(dataset);

	//	timer.setInitialDelay(1000);

		// Sets background color of chart
		chart.setBackgroundPaint(Color.LIGHT_GRAY);

		// Created JPanel to show graph on screen
		final JPanel content = new JPanel(new BorderLayout());

		// Created Chartpanel for chart area
		final ChartPanel chartPanel = new ChartPanel(chart);

		// Added chartpanel to main panel
		content.add(chartPanel);

		// Sets the size of whole window (JPanel)
		chartPanel.setPreferredSize(new java.awt.Dimension(800, 500));
		
		frame.add(chartPanel);
		
		try {
			dc.startUp(filter_OMG);
		} catch (InvalidSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dc.setView2(filter_OMG, this);

	}

	@Override
	public void dispose() {
		super.dispose();
		dc.shutDown();
		this.series.clear();
		this.series= null;
	}

	@Override
	public void setFocus() {
		//mainComposite.setFocus();
	}

	
	
	
	/**
	 * Generates an random entry for a particular call made by time for every 1/4th
	 * of a second.
	 *
	 * @param e the action event.
	 */
	@Override
	public void actionPerformed(final ActionEvent e) {
		this.currenValue  = this.dc.getCryptoService(filter_OMG).getCurrentPrice();

		final Millisecond now = new Millisecond();
		if (this.lastValue!=this.currenValue) {
			this.series.add(new Millisecond(), this.currenValue);
			System.out.println(">>> Current Time in Milliseconds = " + now.toString() + ", Current Value : " + this.currenValue);
			this.lastValue = this.currenValue;
		}
		
	}

	/**
	 * Creates a sample chart.
	 *
	 * @param dataset the dataset.
	 *
	 * @return A sample chart.
	 */
	private JFreeChart createChart(final XYDataset dataset) {
		final JFreeChart result = ChartFactory.createTimeSeriesChart("OMG crypto TimeSeries Chart", "Time",
				"Value", dataset, true, true, false);

		final XYPlot plot = result.getXYPlot();

		plot.setBackgroundPaint(new Color(0xffffe0));
		plot.setDomainGridlinesVisible(true);
		plot.setDomainGridlinePaint(Color.lightGray);
		plot.setRangeGridlinesVisible(true);
		plot.setRangeGridlinePaint(Color.lightGray);
		//ValueMarker marker = new ValueMarker(2, Color.BLACK, new BasicStroke(2.0f));
		//plot.addRangeMarker(marker);
		

        var renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesPaint(0, Color.BLACK);
        renderer.setSeriesStroke(0, new BasicStroke(2.0f));

        plot.setRenderer(renderer);
		

		ValueAxis xaxis = plot.getDomainAxis();
		xaxis.setAutoRange(true);

		// Domain axis would show data of 60 seconds for a time
		xaxis.setFixedAutoRange(60000.0*60); // 60 seconds
		xaxis.setVerticalTickLabels(true);

		ValueAxis yaxis = plot.getRangeAxis();
		yaxis.setRange(0.0, 1000);

		return result;
	}

	@Override
	public void appendText(String string) {
		System.out.println("String "+string);
		
	}

}
