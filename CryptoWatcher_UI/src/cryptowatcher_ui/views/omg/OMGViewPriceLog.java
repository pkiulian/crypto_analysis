package cryptowatcher_ui.views.omg;

import java.awt.event.ActionEvent;

import javax.inject.Inject;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;
import org.osgi.framework.InvalidSyntaxException;

import cryptowatcher_ui.PriceChanger;
import cryptowatcher_ui.views.IViewPartAdapter;

public class OMGViewPriceLog extends ViewPart implements IViewPartAdapter{
	public static final String ID = "cryptowatcher_ui.omgviewpricelog";

	private Composite mainComposite;
	private Text text;

	private String filter_OMG = "(coinId=omg-omg-network)";
	
	@Inject PriceChanger dc;

	@Override
	public void createPartControl(Composite parent) {	
		// create a FormLayout and set its margin
		mainComposite = new Composite(parent, SWT.NONE);
		FormLayout layout = new FormLayout();
		layout.marginHeight = 10;
		layout.marginWidth = 10;

		// set layout for parent
		mainComposite.setLayout(layout);
		
		manageText();
		
		
		dc.setView1(filter_OMG, this);		
		try {
			dc.startUp(filter_OMG);
		} catch (InvalidSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.text.append(""+this.text.hashCode()+" Current OMG price: " + this.dc.getCurrentPrice(filter_OMG) + " \n ");
	}

	@Override
	public void dispose() {
		super.dispose();
		dc.shutDown();
	}

	private void manageText() {
		text = new Text(mainComposite, SWT.READ_ONLY | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		RGB rgb = new RGB(225, 225, 204);
		Color color = new Color(rgb);
		text.setBackground(color);
		// Set up the position of the button on the mainComposite
		FormData positionAndSizeButton = new FormData();
		positionAndSizeButton.top = new FormAttachment(0, 0);
		positionAndSizeButton.bottom = new FormAttachment(100, 0);
		positionAndSizeButton.left = new FormAttachment(0, 0);
		positionAndSizeButton.right = new FormAttachment(100, 0);
			
		// set positionAndSizeButton for button
		text.setLayoutData(positionAndSizeButton);
	}


	@Override
	public void setFocus() {
		mainComposite.setFocus();
	}

	@Override
	public void appendText(String string) {		
		text.append(string);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println("Action Command "+e.getActionCommand());		
		
	}

	

}