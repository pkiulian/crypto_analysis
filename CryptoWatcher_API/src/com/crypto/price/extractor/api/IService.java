package com.crypto.price.extractor.api;


import org.osgi.service.component.ComponentContext;

public interface IService {
	
	/**
	 * @param currency can be EUD, USD. The default value is EUD
	 */
	public void setCurrenncyPrice(String currency);
	
	/**
	 * @return the current price based on the currency
	 */
	public double getCurrentPrice();
	
	/**
	 * The service starts getting the API results and updating the price
	 */
	public void startUpService( ComponentContext context);
	
	/**
	 * Stops the service from quering the API
	 */
	public void shutDownService();


	void addListener(IServiceListener listener);

	void removeListener(IServiceListener listener);
	
	
}
